<html lang="es" ng-app="Manager">
  <head>
  <meta http-equiv="content-type" content="text/html; charset=windows-1250">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
      <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="css/dashboard.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css" rel="stylesheet">

      <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
      <script type="text/javascript" src="js/bootstrap.min.js"></script>
      <script type="text/javascript" src="js/angular.min.js"></script>
     
      <script type="text/javascript" src="js/ui-bootstrap-tpls-2.5.0.min.js"></script>
      <script type="text/javascript" src="angular/angular-route.min.js"></script>
      <script type="text/javascript" src="angular/angular-animate.min.js"></script>
      <script type="text/javascript" src="angular/angular-touch.min.js"></script>
      <script type="text/javascript" src="angular/angular-sanitize.min.js"></script>
       <script type="text/javascript" src="js/loginCtrl.js"></script> 
    
  <title>Escuelas</title>
  <base href="/testescuelas/">
  </head>
  <body>
        <style>
    body {
  padding-top: 40px;
  padding-bottom: 40px;
  background-color: #eee;
}

.form-signin {
  max-width: 330px;
  padding: 15px;
  margin: 0 auto;
}
.form-signin .form-signin-heading,
.form-signin .checkbox {
  margin-bottom: 10px;
}
.form-signin .checkbox {
  font-weight: normal;
}
.form-signin .form-control {
  position: relative;
  height: auto;
  -webkit-box-sizing: border-box;
          box-sizing: border-box;
  padding: 10px;
  font-size: 16px;
}
.form-signin .form-control:focus {
  z-index: 2;
}
.form-signin input[type="email"] {
  margin-bottom: -1px;
  border-bottom-right-radius: 0;
  border-bottom-left-radius: 0;
}
.form-signin input[type="password"] {
  margin-bottom: 10px;
  border-top-left-radius: 0;
  border-top-right-radius: 0;
}

</style>
<div ng-controller="MainCtrl">
<div ng-view class="[onload: string;] [autoscroll: string;]">
          
            
</div>
</div>
  </body>
</html>