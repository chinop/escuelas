<?php
    require_once("loader.php");
    $jSon["ok"]=false;
    $jSon["tabla"]="";
    $jSon["error"]="";
    $jSon["console"]="Session no Cerrada";
    session_start();
    session_destroy();
    if(!session_id())
    {
        $jSon["ok"] = true;
        $jSon["console"] = "Session Cerrada";
    }
    print_r(json_encode($jSon));
?>