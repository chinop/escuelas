<?php
    require_once("../loader.php");
    session_start();
    $jSon["ok"]=false;
    $jSon["tabla"]="";
    $jSon["error"]="";
    $jSon["console"]="";
    if($_SESSION['sessionIniciada']){
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        
        $sql = "SELECT horario.*,aulas.nombre as aulaNombre, grupos.nombre as grupoNombre, agenda.nombre as nombreMaestro FROM horario
                LEFT JOIN personal ON personal.id = horario.maestro
                LEFT JOIN agenda ON agenda.id = personal.agenda
                INNER JOIN aulas ON aulas.id = horario.aula
                INNER JOIN grupos ON grupos.id = horario.grupo
                INNER JOIN escuela ON escuela.id = aulas.escuela AND escuela.id = grupos.escuela";
        $resp = $DataBase2->consultar(null,null,null,$sql);
        if($resp["ok"])
        {
            $jSon["ok"] = true;
            $jSon["tabla"] =  $resp["tabla"];
        }else{
            $jSon =  $resp;
        }
    }else{
        $jSon["error"]="No esta iniciada una session";
    }
    print_r(json_encode($jSon));
?>