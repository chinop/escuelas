<?php
    require_once("../loader.php");
    session_start();
    $jSon["ok"]=false;
    $jSon["tabla"]="";
    $jSon["error"]="";
    $jSon["console"]="";
    if($_SESSION['sessionIniciada']){
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        $sql = "SELECT personal.*, agenda.nombre, agenda.apellido, agenda.correo, agenda.telefono FROM personal
                INNER JOIN agenda ON agenda.id = personal.agenda
                WHERE personal.escuela = '$request->escuela'";
        if(isset($request->maestros)){
            $sql = $sql.'AND personal.materia!=""';
        }
        $resp = $DataBase2->consultar(null,null,null,$sql);
        if($resp["ok"])
        {
            $jSon["ok"] = true;
            $jSon["tabla"] =  $resp["tabla"];
        }else{
            $jSon =  $resp;
        }
    }else{
        $jSon["error"]="No esta iniciada una session";
    }
    print_r(json_encode($jSon));
?>