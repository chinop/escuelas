<?php

class DataBase {
    
    private $host; //= "localhost";
    private $db; //= "quimisondb2";
    private $us; //= "root";
    private $ps; //= "123";
   /* private $host = "mysql1.000webhost.com";
    private $db = "a9250966_quimis";
    private $us = "a9250966_test";
    private $ps = "test123";*/
    
    public  $parametros = array();


    private $res = array();
	
    public $affecedRows = 0;
	
    function __construct($host,$user,$pass,$database)
    {
        $this->host = $host;
        $this->db = $database;
        $this->us = $user;
        $this->ps = $pass;
    }
    
    public function conectar()
    {
        $cn = @new mysqli($this->host,$this->us,$this->ps,$this->db);
        return $cn;
    }
	
	
    public function execute($sql)
    {
        $res["ok"] = false;
        $cn = $this->conectar();
        if(!mysqli_connect_errno()){
            mysqli_set_charset($cn,'utf8');
            if($cn->query($sql)){
                $this->affecedRows = mysqli_affected_rows($cn);
                $res["ok"] = true;                
            }
            else{
                $res["ok"] = false;
                $res["error"] = $cn->error;
            }
        }
        else{
            $res["ok"] = false;
            $res["error"] = mysqli_connect_error();
        }
        return $res;
    }
	
	
    public function consultar($tabla,$campos,$where,$query)
    {
        if(!isset($query)){
            if($where!=""){
                $query = "SELECT ".$campos." FROM ".$tabla." WHERE " . $where;
            }else{
                $query = "SELECT ".$campos." FROM ".$tabla;
            }
        }
            
            
            $res["ok"]=FALSE;
            $res["tabla"]=array();
            $res["error"]="";
          
            $cn= $this->conectar();
            if (!mysqli_connect_errno())
            {
                mysqli_set_charset($cn,'utf8');
                    $rs=$cn->query($query);
                    if ($rs)
                    {
                            while($ren=$rs->fetch_array())
                            {
                                    $res["tabla"][]=$ren;
                            }
                            $res["ok"]=TRUE;
                            $rs->close();
                    }else{
                            $res["ok"]=FALSE;
                            $res["error"]=$cn->error;
                          

                    }
                    $cn->close();
            }else{
                    $res["error"]= mysqli_connect_error();
                    
            }
            return $res;
    }
	public function insertar($tabla,$campos,$values)
    {
		$query = "";                
		if(isset($campos) && $campos!="")
        {
            $query = "INSERT INTO ".$tabla." (".$campos.") VALUES (".$values.")";
        }
		else
        {
            $query = "INSERT INTO ".$tabla."  VALUES (".$values.")";
        }		
		
		$res = $this->execute($query);
		
		/*if($res["ok"]==true){ return true;}
		else{return $res["error"];}*/
		return  $res;
	}
	public function modificar($tabla,$valores,$where){
		$query = "UPDATE ".$tabla." SET ".$valores." WHERE ".$where."";
			
		
		$res = $this->execute($query);
		
		/*if($res["ok"]==true){ return true;}
		else{return $res["error"];}*/
		return  $res;
	}
	public function eliminar($tabla,$where)
    {
		$query = "DELETE FROM ".$tabla." WHERE ".$where."";
					
		$res = $this->execute($query);
		
		return  $res;
	}
    
    public function executeSP_toJSON($nombre)
    {
        $cadena = "(";
        if($this->parametros)
        {
            foreach ($this->parametros as $key => $value) {
                if(is_integer($value))
                {
                    $cadena.=$value.",";
                }
                else{
                    $cadena.="'".$value."',";
                }

            }
            $cadena = substr($cadena, 0, strlen($cadena)-1);
        }
        $cadena .=")";
        $cadena = "call ".$nombre.$cadena;
        $cn = $this->conectar();
        $res = "";

        if(!mysqli_connect_errno()){
            $resul = $cn->query($cadena);

            if($resul){
                while($ren=$resul->fetch_array())
                {
                        $res["tabla"][]=$ren;                            
                }
                $res["ok"]=TRUE;
                $resul->close();
            }
            else{
                $res["ok"] = false;
                $res["error"] = $cn->error;               
            }                
        }
        else{
            $res["ok"] = false;
            $res["error"] = mysqli_connect_error();
        }

        return json_encode($res);

        //return $cadena;
     }
    
     public function executeSP_toTable($nombre)
     {
        $cadena = "(";
        if($this->parametros)
        {
            foreach ($this->parametros as $key => $value) {
                if(is_integer($value))
                {
                    $cadena.=$value.",";
                }
                else{
                    $cadena.="'".$value."',";
                }

            }
            $cadena = substr($cadena, 0, strlen($cadena)-1);
        }
        $cadena .=")";
        $cadena = "call ".$nombre.$cadena;
        $cn = $this->conectar();
        $res = "";

        if(!mysqli_connect_errno()){
            $resul = $cn->query($cadena);

            if($resul){
                while($ren=$resul->fetch_array())
                {
                        $res["tabla"][]=$ren;                            
                }
                $res["ok"]=TRUE;
                $resul->close();
            }
            else{
                $res["ok"] = false;
                $res["error"] = $cn->error;               
            }                
        }
        else{
            $res["ok"] = false;
            $res["error"] = mysqli_connect_error();
        }

        return $res;

        //return $cadena;
    }
        
    public function executeSP_NonQuery($nombre)
    {
        $cadena = "(";
        if($this->parametros)
        {
            foreach ($this->parametros as $key => $value) {
                if(is_integer($value))
                {
                    $cadena.=$value.",";
                }
                else{
                    $cadena.="'".$value."',";
                }

            }
            $cadena = substr($cadena, 0, strlen($cadena)-1);
        }
        $cadena .=")";
        $cadena = "call ".$nombre.$cadena;
        $res = $this->execute($cadena);            
        return $res;            
        //return $cadena;
    }
    
    function agregarParametros($posteo,$negados)
    {
    
        foreach ($posteo as $key => $value) {
            $val = true;
            if($negados){
                foreach($negados as $key2 => $value2){
                    if($key == $value2){
                        $val = false;
                    }
                }
            }            
            if($val){
                $tipo = explode("_",$key);
                if($tipo[0] == "i"){
                    $this->parametros[] = (int)$value;
                }
                elseif($tipo[0] == "d"){
                    $this->parametros[] = (double)$value;
                }
                else{
                    $this->parametros[] = (string)$value;
                }
            }
             
        }         
    }
    
    /*function sp($nomStored,$params)
    {
        $res["ok"]=FALSE;
        $res["tabla"]=array();
        $res["error"]="";
        $cadenak = "";
        $cn= $this->conectar();
        $res = $cn->stmt_init();
        foreach($params as $key => $item)
        {
            $cadenak .= "':".$key."".",";
        }
        
        $cadenak = substr($cadenak, 0, -1);
        echo $cadenak."<br>";
        
        $sql = "call $nomStored($cadenak)";
        echo $sql."<br>";
        
        if(!$res = $cn->prepare($sql))
        {
              echo 'Error: ' . $cn->error;
              return false; // throw exception, die(), exit, whatever...
        } else 
        {
          // the rest of your code
        }
        var_dump($res);
        
        $cadena1 = "";
        $cadena2 = "";
        foreach($params as $key => $item)
        {
            $cadena1 .= ":$key,";
            $cadena2 .= "'$item',";
            echo $key."=>".$item;
            if(is_int($item))
                $param = PDO::PARAM_INT;
            elseif(is_bool($item))
                $param = PDO::PARAM_BOOL;
            elseif(is_null($item))
                $param = PDO::PARAM_NULL;
            elseif(is_string($item))
                $param = PDO::PARAM_STR;
            else
                $param = FALSE;
            if($param)
            {
                $res->bind_param(":".$key."'", $item, $param);
            }
        }
        //$cadena1= substr($cadena1, 0, -1);
        //$cadena2 = substr($cadena2, 0, -1);
        //$res->bind_param($cadena1, $cadena2);
        if(!$res->execute())
        {
            echo 'Error: ' . $cn->error;
            return false; // throw exception, die(), exit, whatever...
        } else 
        {
            return $res;
        }
          
        
    }*/
    
    function StoredProcedure($nomStored,$params)
    {
        $res["ok"]=FALSE;
        $res["tabla"]=array();
        $res["error"]="";
        $cadenak = "";
        $cadena1 = "";
        $cadena2 = "";
        $cn = $this->conectar();
        if($cn->connect_errno!=0)
        {
            $res["error"] = 'Error: ' . $cn->connect_error;
            return $res; // throw exception, die(), exit, whatever...
        }
        else
        {
            $stmt = $cn->stmt_init();
            foreach($params as $key => $item)
            {
                $cadenak .= "?,";
                //echo $item."</br>";
                if(is_int($item))
                    $cadena1 .= "i";
                elseif(is_bool($item))
                    $cadena1 .= "d";
                elseif(is_null($item))
                    $cadena1 .= "n";
                elseif(is_string($item))
                    $cadena1 .= "s";
            }

            $cadenak = substr($cadenak, 0, -1);
            //echo $cadena1."aloha<br>";
            $params = $this->getRefArray($params); // <-- Added due to changes since PHP 5.3
            array_unshift($params,$cadena1);
            //$sql = "INSERT INTO receta(nombre,Tplatillo,Tcocina,Tpreparacion,Tcoccion,imgPlatillo,creadoEl) VALUES($cadenak,NOW())";
            $sql = "call $nomStored($cadenak)";
            //echo "<br>".$sql."<br>";

            if(!$stmt = $cn->prepare($sql))
            {
                $res["error"] = 'Error: ' . $cn->error;
                return $res; // throw exception, die(), exit, whatever...
            } 
            else 
            {
                //$params = getRefArray($params); // <-- Added due to changes since PHP 5.3 

                //echo "http://php.net/mysqli-stmt.bind-param"."-------"."";
                $method = @new ReflectionMethod('mysqli_stmt', 'bind_param');
                //var_dump($method);
                $method->invokeArgs($stmt, $params);

                if(!$stmt->execute())
                {
                    $res["error"] = 'Error: ' . $cn->error;
                    return $res; // throw exception, die(), exit, whatever...
                } 
                else 
                {
                    $tablas = array();
                    $meta = $stmt->get_result();
                    
                    while($stmt->more_results())
                    {
                        $stmt->next_result();
                        while ($field = $meta->fetch_array())
                        {
                            $tablas[] = $field;
                        }
                        $meta = $stmt->get_result();
                    }
                    /*echo "\n<br>--------------------------------------\n<br>";
                    $meta = $stmt->get_result();
                    //var_dump($stmt);
                    //var_dump($meta);
                    while ($field = $meta->fetch_array())
                    {
                        var_dump($field);
                    }
                    // This is the tricky bit dynamically creating an array of variables to use
                    // to bind the results
                    //echo "inicio del while<br>";
                    while ($field = $meta->fetch_assoc())
                    {
                        echo "aloha<br>";
                        var_dump($field);//-----terminar arreglo diferente
                        $var = $field->name; 
                        $$var = null; 
                        $fields[$var] = &$$var;
                    }
                    echo "fin del while<br>";
                    var_dump($fields);
                    $method = @new ReflectionMethod('mysqli_stmt', 'bind_result'); 
                    $method->invokeArgs($stmt, $fields);

                    $i = 0;
                    var_dump($stmt);
                    while ($stmt->fetch()) 
                    {
                        $results[$i] = array();
                        $a = 0;
                        foreach($fields as $k => $v)
                        {
                            $results[$i][$a] = $v;
                            $results[$i][$k] = $v;
                            $a++;
                        }
                        $i++;
                    }*/
                    $stmt->close();
                    $cn->close();
                    $res["tabla"] = $tablas;
                    $res["ok"]=true;
                    return $res;
                }
            }
        }
    }
    
    private function getRefArray($a) 
    { 
        if (strnatcmp(phpversion(),'5.3')>=0) 
        { 
            $ret = array(); 
            foreach($a as $key => $val) 
            { 
                $ret[$key] = &$a[$key]; 
            } 
            return $ret; 
        } 
        return $a; 
    }    
}

?>
