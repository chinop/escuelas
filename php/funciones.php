<?php

function exe_query($Query)
{
    global $DataBase;
    $resp = $DataBase->exeQuery($Query);
    return $resp;
}


function fetch_query($Query)
{
    global $DataBase;
    $resp = $DataBase->fetchArrayMySql($Query);
    return $resp;
}


function UPloadIMG($imgName,$imgTMP_name,$imgSize)
    {
        $target_dir = "/img/img_recetas/";
        $target_file = $target_dir . basename($imgName);
        //var_dump($target_file);
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        $check = getimagesize($imgTMP_name);
        if($check !== false) {
            echo "File is an image - " . $check["mime"] . ".";
            $uploadOk = 1;
            // Allow certain file formats
            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") 
            {
                echo "Sorry, only JPG, JPEG, & PNG files are allowed.";
                $uploadOk = 0;
            }
            else
            {
                // Check file size
                if ($imgSize > 5300000) 
                {
                    echo "Sorry, your file is too large.";
                    $uploadOk = 0;
                }
            }
        }
        else
        {
            echo "File is not an image.";
            $uploadOk = 0;
        }
        
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) 
        {
            echo "Sorry, your file was not uploaded.";
        // if everything is ok, try to upload file
        } else {
            //var_dump($target_file);
            $x = explode(".",$target_file);
            $count = count($x);
            $count--;
            unset($x[$count]);
            $target_file = implode(".", $x);
            $target_file = "..".$target_file.".jpg";
            //var_dump($target_file);
            if (move_uploaded_file($imgTMP_name, $target_file)) 
            {
                LogConsole("The file ". basename($imgName). " has been uploaded.");
                 return $target_file;
            } else {
                echo "Sorry, there was an error uploading your file.";
            }
        }
    }


    function randKey($length){
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }






















?>