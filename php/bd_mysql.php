<?php
class MyBD
{
    private $conn;
    private $mybd_host;
    private $mybd_database;
    private $mybd_user;
    private $mybd_password;
    
    function __construct($host,$user,$pass,$database)
    {
        $this->mybd_database = $database;
        $this->mybd_host = $host;
        $this->mybd_user = $user;
        $this->mybd_password = $pass;
    }
    
    function Conectar()
    {
        $this->conn = @new mysqli($this->mybd_host,
            $this->mybd_user,
            $this->mybd_password,
            $this->mybd_database); /*mysqli_connect(
            $this->mybd_host,
            $this->mybd_user,
            $this->mybd_password,
            $this->mybd_database) or die ("Error" .mysqli_error($this->conn));*/
        //var_dump($this->conn);
        return $this->conn;
    }
    
    function Cerrar()
    {
        $this->conn = mysqli_close($this->conn);
        return true;
    }
    
    function exeQuery($sql)
    {
        $conn = $this->Conectar();
        mysqli_set_charset($conn,'utf8');
        if($conn)
        {
            $result = $conn->query($sql);
            $conn = $this->Cerrar();
        }
        else
            $result = false;
        return $result;
    }
    
    function fetchArrayMySql($sql)
    {
        $result = $this->exeQuery($sql);
        while ($row = $result->_fetch_row($MyTabla))
        {
            var_dump($row);
        }
        var_dump($result);
        $result = mysqli_fetch_row($result);
        return $result;
    }

}
?>