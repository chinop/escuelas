var Master = angular.module('Manager',['ngRoute']);
    Master.config(function($routeProvider, $locationProvider) {
      $routeProvider
    .when('/RegUsuario',{
        templateUrl: 'views/registrarusuario.html',
        controller:'RegCtrl'
      })
    .when('/Login',{
        templateUrl: 'views/login_view.html',
        controller:'LogInCtrl'
      })
    .otherwise({
               redirectTo: '/Login'//cambiar luego a raiz
            });
    $locationProvider.html5Mode(true);
    });
Master.controller('MainCtrl', function($scope, $route, $routeParams, $location) {
     $scope.$route = $route;
     $scope.$location = $location;
     $scope.$routeParams = $routeParams;
 });
Master.controller('LogInCtrl',function ($scope,$http) {
    $scope.Login = function(){
        var request = $http({
            method: "POST",
            url: 'php/LogIn.php',
            data: {user:$scope.user,pssw:$scope.pssw}
            }).then(function(success){
                console.log(success.data)
                
                success.data;
                if(success.data["ok"]){
                    location.reload();
                }else{
                    alert(success.data["error"]);
                }
            },function(err){
                console.log(err.data)
                
                alert("Error :"+err.data["error"]);
                console.error(err.data["error"]);
            });
    }
});

Master.controller('RegCtrl',function ($scope, $http, $location) {
    $scope.registro = {'user': "",'pssw': "",'nombre': "",'apellido': "",'correo': "",'telefono': ""}
    $scope.formShow = true
    $scope.ejemplo = "aloha loguin";
    $scope.registrarUsuario = function(){
        if (!$scope.frmReg.$valid){
            console.log($scope.frmReg);
        } else {
            var request = $http({
            method: "POST",
            url: 'php/alta/usuario.php',
            data: $scope.registro
            }).then(function(success){
                
                success.data;
                if(success.data["ok"]){
                    console.log("termino agregar usuario");
                    $location.path("/Login");
                }
            },function(err){
                
                console.error(err.data["error"]);
            });
        }
    }
});