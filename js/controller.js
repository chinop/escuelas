var Master = angular.module('Manager',['ngRoute','ui.bootstrap']).service('Fns', function($uibModal) {

		this.Modal = function(temp, Ctrl, res, size, cb) {
			var modalInstance;
			if (!size) {
				size = 'md';
			}
			if (!res) {
				res = {};
			}
			modalInstance = $uibModal.open({
        animation:false,
				templateUrl: temp,
				controller: Ctrl,
				size: size,
				resolve: res
			});
			if (cb) {

				modalInstance.result.then(cb, function(){
					cb()
				}).finally(function(cb){
					modalInstance.$destroy(); 
					// After calling this, the next time open is called, everything will be re-initialized.
				});
			}
			return modalInstance;
		};
  });
  
    Master.config(function($routeProvider, $locationProvider) {
      $routeProvider
    .when('/Escuelas',{
        templateUrl: 'views/escuelas.html',
        controller:'EscuelasCtrl'
      })
    .when('/Escuela/:escuelaId',{
        templateUrl: 'views/escuela.html',
        controller:'Escuela_Ctrl'
      })
    .when('/Escuela/:escuelaId/personal',{
        templateUrl: 'views/personal.html',
        controller:'PersonalCtrl'
      })
      .when('/Escuela/:escuelaId/alumnos',{
        templateUrl: 'views/alumnos.html',
        controller:'AlumnosCtrl'
      })
      .when('/Escuela/:escuelaId/aulas',{
        templateUrl: 'views/aulas.html',
        controller:'AulasCtrl'
      })
      .when('/Escuela/:escuelaId/grupos',{
        templateUrl: 'views/grupos.html',
        controller:'GruposCtrl'
      })
      .when('/Escuela/:escuelaId/horarios',{
        templateUrl: 'views/horarios.html',
        controller:'HorariosCtrl'
      })
    .when('/Bienvenido',{
        templateUrl: 'views/dashboard.html'
      })
    .otherwise({
               redirectTo: '/Bienvenido'//cambiar luego a raiz
            });
    $locationProvider.html5Mode(true);
    });

Master.controller('MainCtrl', function($scope, $route, $routeParams, $location, $http) {
     $scope.$route = $route;
     $scope.$location = $location;
     $scope.$routeParams = $routeParams;
     $scope.aloha = function(){
       alert("aloha")
     }
     $scope.Logout = function(){
       $http({
            method: "POST",
            url: 'php/LogOut.php',
            data: ""
            }).then(function(success){
                success.data;
                if(success.data["ok"]){
                  alert(success.data["console"]);
                    location.reload();
                }
            },function(err){
                console.error(err.data["error"]);
                alert("Error en la base de datos");
            });
     }
 });

Master.controller('EscuelasCtrl',function ($scope, $routeParams, $http, Fns) {
  $scope.ejemplo = $routeParams;
  $scope.escuelas = [];

  $scope.agregarEsc = function(){
    var data ={editando:function(){return false},};
    Fns.Modal('views/forms/escuela.html','EditEscCtrl',data,null,$scope.cargarEscuelas());
  }
  $scope.editar = function(item){
    var data ={
      editando:function(){return true},
      id:function(){ return item.id},
      nombre:function(){ return item.nombre},
      direccion:function(){ return item.direccion},
      servicio:function(){ return item.servicio},
      directoractual:function(){ return item.directoractual},
      telefono:function(){ return item.telefono},
      estado:function(){ return item.estado},
      ciudad:function(){ return item.ciudad},
      clave:function(){ return item.clave}
    };
    Fns.Modal('views/forms/escuela.html','EditEscCtrl',data,null,$scope.cargarEscuelas());
  }
  $scope.cargarEscuelas = function(){
    $http({
            method: "POST",
            url: 'php/selects/escuelas.php',
            data: ""
            }).then(function(success){
                success.data;
                if(success.data["ok"]){
                  $scope.escuelas = success.data.tabla;
                }
            },function(err){
                console.error(err.data["error"]);
                alert("Error en la base de datos");
            });
  }
  $scope.cargarEscuelas();
  $scope.eliminar = function(id){
    
    if(confirm("desea eliminar este registro")){
      $http({
        method: "POST",
        url: 'php/elim/escuela.php',
        data: {id:id}
        }).then(function(success){
          
            success.data;
            if(success.data["ok"]){
              $scope.cargarEscuelas();
              alert("se a eliminado con exito");

            }
        },function(err){
          
            console.error(err.data["error"]);
            alert("Error en la base de datos");
      });
    }
  }
});

Master.controller('EditEscCtrl', function($scope, $routeParams, $uibModalInstance, $http){
  $scope.registro = {id:"",nombre:"",direccion:"",servicio:"",directoractual:"",telefono:"",estado:"",ciudad:"",clave:""}
  if($scope.$resolve.id){
    $scope.registro = {id:$scope.$resolve.id,
      nombre:$scope.$resolve.nombre,
      direccion:$scope.$resolve.direccion,
      servicio:$scope.$resolve.servicio,
      directoractual:$scope.$resolve.directoractual,
      telefono:$scope.$resolve.telefono,
      estado:$scope.$resolve.estado,
      ciudad:$scope.$resolve.ciudad,
      clave:$scope.$resolve.clave}
  }
  $scope.cerrarM = function(){
    $uibModalInstance.dismiss('cancel');
  }
  $scope.guardar = function(){
    
    console.log($scope.registro);
    if (!$scope.frmReg.$valid){
        console.log($scope.frmReg);
    } else {
      $http({
        method: "POST",
        url: url = ($scope.$resolve.editando)?'php/edit/escuela.php':'php/alta/escuela.php',
        data: $scope.registro
        }).then(function(success){
          
            if(success.data["ok"]){
              alert("Se guardo con existo");
              $scope.registro.id =success.data.tabla.id
              $scope.cerrarM();
            }
        },function(err){
            
            console.error(err.data["error"]);
        });
    }
  }
  $scope.personal =[]
  $scope.cargarPersonal = function(){
    debugger
    $http({
      method: "POST",
      url: 'php/selects/personal.php',
      data: {escuela:$scope.$resolve.id}
    }).then(function(success){
      
        success.data;
        if(success.data["ok"]){
          $scope.personal = success.data.tabla;
        }
    },function(err){
        
        console.error(err.data["error"]);
        alert("Error en la base de datos");
    });
  }
  $scope.cargarPersonal();
});

Master.controller('Escuela_Ctrl',function ($scope, $route, $routeParams, $location, $http) {
  $scope.escuela = [];
  $scope.cargarEscuela = function(){
    $http({
            method: "POST",
            url: 'php/selects/escuelas.php',
            data: {id:$routeParams.escuelaId}
            }).then(function(success){
                success.data;
                if(success.data["ok"]){
                  $scope.escuela = success.data.tabla[0];
                }
            },function(err){
                console.error(err.data["error"]);
                alert("Error en la base de datos");
            });
  }
  $scope.cargarEscuela();
});
  
