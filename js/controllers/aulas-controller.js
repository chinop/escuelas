Master.controller('AulasCtrl',function($scope,$http,$routeParams,Fns){
  $scope.escuela = [];
  $scope.cargarEscuela = function(){
    $http({
            method: "POST",
            url: 'php/selects/escuelas.php',
            data: {id:$routeParams.escuelaId}
            }).then(function(success){
                success.data;
                if(success.data["ok"]){
                  $scope.escuela = success.data.tabla[0];
                }
            },function(err){
                console.error(err.data["error"]);
                alert("Error en la base de datos");
            });
  }
  $scope.cargarEscuela();
  $scope.aulas = [];
  $scope.cargarAulas = function(){
    
    $http({
            method: "POST",
            url: 'php/selects/aulas.php',
            data: {escuela:$routeParams.escuelaId}
            }).then(function(success){
                console.log(success.data);
                if(success.data["ok"]){
                  $scope.aulas = success.data.tabla;
                }
            },function(err){
                console.error(err.data["error"]);
                alert("Error en la base de datos");
            });
  }
  $scope.cargarAulas();

  $scope.addAulas = function(){
      data={
          editando:false,
          escuela:function(){ return $routeParams.escuelaId}
      }
    Fns.Modal('views/forms/aula.html','aula_modal',data);
  }
  $scope.editar = function(item){
    var data ={
      editando:function(){return true},
      id:function(){ return item.id},
      escuela:function(){ return $routeParams.escuelaId},
      nombre:function(){ return item.nombre}
    };
    Fns.Modal('views/forms/aula.html','aula_modal',data,null,$scope.cargarAulas());
  }
  $scope.eliminar = function(_id){
    if(confirm("desea eliminar este registro")){
      
      $http({
        method: "POST",
        url: 'php/elim/aula.php',
        data: {id:_id}
        }).then(function(success){
          
            success.data;
            if(success.data["ok"]){
              $scope.cargarAulas();
              alert("se a eliminado con exito");
            }
        },function(err){
          
            console.error(err.data["error"]);
            alert("Error en la base de datos");
      });
    }
  }

});
Master.controller('aula_modal',function($scope, $uibModalInstance, $http){
  $scope.editando = $scope.$resolve.editando;
  $scope.cerrarM = function(){
      $uibModalInstance.dismiss('cancel');
  }
  $scope.registro = {id:"",escuela:$scope.$resolve.escuela,nombre:""}
  if($scope.$resolve.id){
    $scope.registro = {id:$scope.$resolve.id,escuela:$scope.$resolve.escuela,nombre:$scope.$resolve.nombre}
  }
  $scope.guardar = function(){
    console.log($scope.registro);
    
    if (!$scope.frmReg.$valid){
        console.log($scope.frmReg);
    } else {
      $http({
        method: "POST",
        url: url = ($scope.$resolve.editando)?'php/edit/aula.php':'php/alta/aula.php',
        data: $scope.registro
        }).then(function(success){
          
            if(success.data["ok"]){
              alert("Se guardo con exito");
              $scope.cerrarM();
            }
        },function(err){
            
            console.error(err.data["error"]);
        });
    }
  }

});