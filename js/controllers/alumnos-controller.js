Master.controller('AlumnosCtrl',function($scope,$http,$routeParams,Fns){
  $scope.escuela = [];
  $scope.cargarEscuela = function(){
    $http({
            method: "POST",
            url: 'php/selects/escuelas.php',
            data: {id:$routeParams.escuelaId}
            }).then(function(success){
                success.data;
                if(success.data["ok"]){
                  $scope.escuela = success.data.tabla[0];
                }
            },function(err){
                console.error(err.data["error"]);
                alert("Error en la base de datos");
            });
  }
  $scope.cargarEscuela();
  $scope.alumnos = [];
  $scope.cargarAlumnos = function(){
    $http({
            method: "POST",
            url: 'php/selects/alumnos.php',
            data: {escuela:$routeParams.escuelaId}
            }).then(function(success){
                console.log(success.data);
                if(success.data["ok"]){
                  $scope.alumnos = success.data.tabla;
                }
            },function(err){
                console.error(err.data["error"]);
                alert("Error en la base de datos");
            });
  }
  $scope.cargarAlumnos();

  $scope.addPersona = function(){
      data={
          editando:false,
          escuela:function(){ return $routeParams.escuelaId}
      }
    Fns.Modal('views/forms/alumno.html','alumnos_modal',data);
  }
  $scope.editar = function(item){
    var data ={
      editando:function(){return true},
      id:function(){ return item.id},
      agenda:function(){ return item.agenda},
      escuela:function(){ return $routeParams.escuelaId},
      nombre:function(){ return item.nombre},
      apellido:function(){ return item.apellido},
      correo:function(){ return item.correo},
      telefono:function(){ return item.telefono},
      grupo:function(){ return item.grupo},
      sangre:function(){ return item.sangre},
      alergias:function(){ return item.alergias}
    };
    Fns.Modal('views/forms/alumno.html','alumnos_modal',data,null,$scope.cargarAlumnos());
  }
  $scope.eliminar = function(agenda){
    if(confirm("desea eliminar este registro")){
      
      $http({
        method: "POST",
        url: 'php/elim/alumno.php',
        data: {agenda:agenda}
        }).then(function(success){
          
            success.data;
            if(success.data["ok"]){
              $scope.cargarAlumnos();
              alert("se a eliminado con exito");
            }
        },function(err){
          
            console.error(err.data["error"]);
            alert("Error en la base de datos");
      });
    }
  }

});
Master.controller('alumnos_modal',function($scope, $uibModalInstance, $http){
  $scope.editando = $scope.$resolve.editando;
  $scope.cerrarM = function(){
      $uibModalInstance.dismiss('cancel');
  }
  $scope.registro = {id:"",agenda:"",escuela:$scope.$resolve.escuela,nombre:"",apellido:"",correo:"",telefono:"",grupo:"",sangre:"",alergias:""}
  if($scope.$resolve.id){
    $scope.registro = {id:$scope.$resolve.id,
    agenda:$scope.$resolve.agenda,
    escuela:$scope.$resolve.escuela,
    nombre:$scope.$resolve.nombre,
    apellido:$scope.$resolve.apellido,
    correo:$scope.$resolve.correo,
    telefono:$scope.$resolve.telefono,
    grupo:$scope.$resolve.grupo,
    sangre:$scope.$resolve.sangre,
    alergias:$scope.$resolve.alergias}
  }
  $scope.guardar = function(){
    console.log($scope.registro);
    
    if (!$scope.frmReg.$valid){
        console.log($scope.frmReg);
    } else {
      $http({
        method: "POST",
        url: url = ($scope.$resolve.editando)?'php/edit/alumno.php':'php/alta/alumno.php',
        data: $scope.registro
        }).then(function(success){
          
            if(success.data["ok"]){
              alert("Se guardo con exito");
              $scope.cerrarM();
            }
        },function(err){
            
            console.error(err.data["error"]);
        });
    }
  }

});