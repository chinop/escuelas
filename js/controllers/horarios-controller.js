Master.controller('HorariosCtrl',function($scope,$http,$routeParams,Fns){
  $scope.escuela = [];
  $scope.cargarEscuela = function(){
    $http({
            method: "POST",
            url: 'php/selects/escuelas.php',
            data: {id:$routeParams.escuelaId}
            }).then(function(success){
                success.data;
                if(success.data["ok"]){
                  $scope.escuela = success.data.tabla[0];
                }
            },function(err){
                console.error(err.data["error"]);
                alert("Error en la base de datos");
            });
  }
  $scope.cargarEscuela();
  $scope.horarios = [];
  $scope.cargarHorarios = function(){
    $http({
            method: "POST",
            url: 'php/selects/horarios.php',
            data: ""
            }).then(function(success){
                console.log(success.data);
                if(success.data["ok"]){
                  $scope.horarios = success.data.tabla;
                }
            },function(err){
                console.error(err.data["error"]);
                alert("Error en la base de datos");
            });
  }
  $scope.cargarHorarios();

  $scope.addHorario = function(){
      data={
          editando:false,
          escuela:function(){ return $routeParams.escuelaId}
      }
    Fns.Modal('views/forms/horario.html','horario_modal',data);
  }
  $scope.editar = function(item){
    var data ={
      editando:function(){return true},
      escuela:function(){ return $routeParams.escuelaId},
      id:function(){ return item.id},
      maestro:function(){ return item.maestro},
      aula:function(){ return item.aula},
      grupo:function(){ return item.grupo},
      horainicio:function(){ return item.horainicio},
      horafinal:function(){ return item.horafinal},
      dia:function(){ return item.dia}
    };
    Fns.Modal('views/forms/horario.html','horario_modal',data);
  }
  $scope.eliminar = function(_id){
    if(confirm("desea eliminar este registro")){
      
      $http({
        method: "POST",
        url: 'php/elim/horario.php',
        data: {id:_id}
        }).then(function(success){
          
            success.data;
            if(success.data["ok"]){
              $scope.cargarHorarios();
              alert("se a eliminado con exito");
            }
        },function(err){
          
            console.error(err.data["error"]);
            alert("Error en la base de datos");
      });
    }
  }

});
Master.controller('horario_modal',function($scope, $uibModalInstance, $http){
  //-------------AULAS---------------------
  $scope.aulas = [];
  $scope.cargarAulas = function(){
    
    $http({
            method: "POST",
            url: 'php/selects/aulas.php',
            data: {escuela:$scope.$resolve.escuela}
            }).then(function(success){
                console.log(success.data);
                if(success.data["ok"]){
                  $scope.aulas = success.data.tabla;
                }
            },function(err){
                console.error(err.data["error"]);
                alert("Error en la base de datos");
            });
  }
  $scope.cargarAulas();
  //----------GRUPOS------------------------
  $scope.grupos = [];
  $scope.cargarGrupos = function(){
    
    $http({
            method: "POST",
            url: 'php/selects/grupos.php',
            data: {escuela:$scope.$resolve.escuela}
            }).then(function(success){
                console.log(success.data);
                if(success.data["ok"]){
                  $scope.grupos = success.data.tabla;
                }
            },function(err){
                console.error(err.data["error"]);
                alert("Error en la base de datos");
            });
  }
  $scope.cargarGrupos();
  //-----------PERSONAL----------------------
  $scope.personal = [];
  $scope.cargarPersonal = function(){
    $http({
            method: "POST",
            url: 'php/selects/personal.php',
            data: {escuela:$scope.$resolve.escuela,maestros:true}
            }).then(function(success){
                console.log(success.data);
                if(success.data["ok"]){
                  $scope.personal = success.data.tabla;
                }
            },function(err){
                console.error(err.data["error"]);
                alert("Error en la base de datos");
            });
  }
  $scope.cargarPersonal();

  $scope.editando = $scope.$resolve.editando;
  $scope.cerrarM = function(){
      $uibModalInstance.dismiss('cancel');
  }
    $scope.registro = {
        id: "",
        maestro: "",
        aula: "",
        grupo: "",
        horainicio: "",
        horafinal: "",
        dia: ""
    }
    if($scope.$resolve.id){
      debugger
      $scope.registro = {
          id: $scope.$resolve.id,
          maestro: $scope.$resolve.maestro,
          aula: $scope.$resolve.aula,
          grupo: $scope.$resolve.grupo,
          horainicio:moment($scope.$resolve.horainicio)._d,
          horafinal: moment($scope.$resolve.horafinal)._d,
          dia: $scope.$resolve.dia
      }
    }
  $scope.guardar = function(){
    console.log($scope.registro);
    
    if (!$scope.frmReg.$valid){
        console.log($scope.frmReg);
    } else {
      $http({
        method: "POST",
        url: url = ($scope.$resolve.editando)?'php/edit/horario.php':'php/alta/horario.php',
        data: $scope.registro
        }).then(function(success){
          
            if(success.data["ok"]){
              alert("Se guardo con exito");
              $scope.cerrarM();
            }else{
              alert("Error intentelo de nuevo");
              console.error(success.data["error"]);
            }
        },function(err){
            
            console.error(err.data["error"]);
        });
    }
  }
  $scope.getHora = function(date){
    debugger;
    return moment(date).format("h:mm a");
  }
});