<?php session_start();?>
<?php if(isset($_SESSION["sessionIniciada"]) && $_SESSION["sessionIniciada"] !== ""){?>
<!DOCTYPE HTML>
<html lang="es" ng-app="Manager">
  <head>
  <meta http-equiv="content-type" content="text/html; charset=windows-1250">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
      <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="css/dashboard.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css" rel="stylesheet">

      <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
      <script type="text/javascript" src="js/bootstrap.min.js"></script>
      <script type="text/javascript" src="js/angular.min.js"></script>
     
      <script type="text/javascript" src="js/ui-bootstrap-tpls-2.5.0.min.js"></script>
      <script type="text/javascript" src="angular/angular-route.min.js"></script>
      <script type="text/javascript" src="angular/angular-animate.min.js"></script>
      <script type="text/javascript" src="angular/angular-touch.min.js"></script>
      <script type="text/javascript" src="angular/angular-sanitize.min.js"></script>
      <script type="text/javascript" src="https://momentjs.com/downloads/moment.js"></script>
       <script type="text/javascript" src="js/controller.js"></script> 
       <script type="text/javascript" src="js/controllers/personal-controller.js"></script> 
       <script type="text/javascript" src="js/controllers/alumnos-controller.js"></script>
       <script type="text/javascript" src="js/controllers/aulas-controller.js"></script>
       <script type="text/javascript" src="js/controllers/grupos-controller.js"></script>
       <script type="text/javascript" src="js/controllers/horarios-controller.js"></script>

  <title>Escuelas</title>
  <base href="/testescuelas/">
  </head>
  <body>
    <div ng-controller="MainCtrl">
      
      <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
      <a class="navbar-brand" href="/testescuelas">Dashboard</a>
      <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="Escuelas">Escuelas<span class="sr-only">(current)</span></a>
          </li>
        </ul>
          
      </div>
      <button class="btn btn-success" ng-click="Logout()" type="button">Cerrar session</button>
    </nav>

      
      <div class="container-fluid">
      <div class="row">
        <nav class="col-sm-3 col-md-2 d-none d-sm-block bg-light sidebar">
          
        </nav>

        <main class="col-sm-9 ml-sm-auto col-md-10 pt-3" role="main">
          <div ng-view class="[onload: string;] [autoscroll: string;]">
          <!-- Cargado de las vistas  -->
            
          </div>
        </main>
      </div>
    </div>
    
    </div>
  </body>
</html>
 <?php }else {
    include("LogOn.php");
  }?>